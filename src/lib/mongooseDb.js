let mongoose = require('mongoose');
class MongooseDB
{
    constructor(mongoUrl){
        mongoose.connect(mongoUrl);
        mongoose.set('useNewUrlParser', true);
        mongoose.set('useFindAndModify', false);
        mongoose.set('useCreateIndex', true);

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', function ()
        {
            // we're connected!
            console.log(':)) connected to mongodb');
        });
        this.mongoose = mongoose;
        this.schemas = {
            //attach your schema(s) in index.js(they can have seprate files)
        };
    }
}
module.exports = MongooseDB ;
