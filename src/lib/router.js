const path = require('path');
const mustache = require('mustache');
const Jimp = require('jimp');
var expres = require('express');
var express = expres.Router();
const moment = require('moment')
const MONGO_URL = require('../../constant').MONGO_URL
class Router
{
    constructor()
    {
        //exports:
        this.router = express;
        //function binding:
        this.now = this.now.bind(this);
        this.sendResponse = this.sendResponse.bind(this);
        this.makeAdminRouter = this.makeAdminRouter.bind(this);
        this.accessDenied = this.accessDenied.bind(this);
        // this.handleFile = this.handleFile.bind(this);
        // this.handleFileArray = this.handleFileArray.bind(this);
        // this.renderTemplate = this.renderTemplate.bind(this);
    }
    now = () =>
    {
        return moment().format('YYYY-MM-DD hh:mm:ss');
    }
    //message functions:
    sendResponse(req, res, body, code = 200)
    {
        res.status(code).send(body);
    }
    //admin functions:
    makeAdminRouter()
    {
        this.router.use((req, res, next) =>
        {
            if (this.isAdmin(req))
            {
                next();
            }
            else
            {
                console.log('400 due to admin session');
                this.acccessDenied(res);
                return;
            }
        });
    }
    accessDenied(res)
    {
        res.status(400).send({ message: 'Access Denied!', code: 400 });
    }
    //template functions:
    // renderTemplate(fileName, view)
    // {
    //     if (!fileName.includes("public/"))
    //         fileName = "public/" + fileName;
    //     try
    //     {
    //         let view_str = fileSystem.readFileSync(path.resolve(fileName)).toString();
    //         return mustache.render(view_str, view);
    //     } catch (err)
    //     {
    //         return "render file failed =>" + err;
    //     }
    // }
    // //file functions:
    // handleFile(req, res, name, folder = '', sizes = [])
    // {
    //     if (folder != '' && folder.indexOf('/') == -1)
    //         folder += '/';
    //     return new Promise((resolve, reject) =>
    //     {
    //         try
    //         {
    //             // console.log('req.files='+JSON.stringify(req.files));
    //             if (req.files && req.files[name])
    //             {
    //                 let f = req.files[name];
    //                 const fileName = (f.name.substring(0, f.name.lastIndexOf('.')) + '-' + new Date().getTime().toString()).replace(' ', '');
    //                 const fileFormat = f.name.substring(f.name.lastIndexOf('.'), f.name.length);
    //                 const filePath = '../storage/' + folder + fileName + fileFormat;
    //                 console.log(filePath);
    //                 f.mv(filePath, (err) =>
    //                 {
    //                     if (err)
    //                         reject(err);
    //                     else
    //                     {
    //                         console.log('file uploaded ' + filePath);
    //                         if (sizes == undefined || sizes.length == 0)
    //                             resolve({ path: filePath.replace('../storage/','/storage/'), url: API_URL.replace('api/', '') + `${folder + fileName + fileFormat}` });
    //                         else
    //                         {
    //                             for (var i = 0; i < sizes.length; i++)
    //                             {
    //                                 if (sizes[i].width == -1)
    //                                     sizes[i].width = Jimp.AUTO;
    //                                 if (sizes[i].height == -1)
    //                                     sizes[i].height = Jimp.AUTO;
    //                                 const i2 = i;
    //                                 Jimp.read(filePath, (err, img) =>
    //                                 {
    //                                     img
    //                                         .resize(sizes[i2].width, sizes[i2].height)
    //                                         .quality(60)
    //                                         .write('../storage/' + folder + fileName + '-resize-' + sizes[i2].width + 'x' + sizes[i2].height + fileFormat)
    //                                 });
    //                             }
    //                             setTimeout(() =>
    //                             {
    //                                 resolve({ path: filePath.replace('../storage/','/storage/'), url: API_URL.replace('api/', '') + `${folder + fileName + fileFormat}` });
    //                             }, 200 * sizes.length);
    //                         }
    //                     }
    //                 });
    //             }
    //             else
    //             {
    //                 resolve(undefined);
    //             }
    //         }
    //         catch (err)
    //         {
    //             console.log(err);
    //             reject(err);
    //         }
    //     });
    // }
    
}

module.exports = Router;