var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    gender: String,
    phone: String,
    create_date: {
        
        type: Date,
        default: Date.now
    },
    profile_image:String,
    project_ids :{
        type:Array,
        default:[]
    }
});

var Users = module.exports = mongoose.model('user', userSchema);
module.exports.get = function (callback, limit) {
    Users.find(callback).limit(limit);
}