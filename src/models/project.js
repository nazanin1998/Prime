var mongoose = require('mongoose');

var projectSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    }
});
// Export Contact model
var Projects = module.exports = mongoose.model('project', projectSchema);
module.exports.get = function (callback, limit) {
    Projects.find(callback).limit(limit);
}
Projects.Helpers = {
    public: (doc) =>
    {
        doc = doc.toObject();

        return doc;
    },
}