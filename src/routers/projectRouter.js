const APIRouter = require('./apiRoutes')
const isEmptyString = require('./../utils/utils').isEmptyString;
const moment_now = require('./../utils/utils').moment_now;


class ProjectsHttpRouter extends APIRouter
{
    constructor(handler)
    {
        super();
        this.handler = handler;
        this.router.get('/search', (req, res) =>
        {
            let limit = parseInt(req.query.limit ? req.query.limit : 20);
            let offset = parseInt(req.query.offset ? req.query.offset : 0);
            delete(req.query.limit);
            delete(req.query.offset);
            this.handler.search(req.query.s,limit,offset).then((result) =>
            {
                this.sendResponse(req, res, result);
            }).catch((err) =>
            {
                this.handleError(req, res, err);
            });
        });
    }
}

class ProjectsHandler
{
     constructor(Project)
    {
        this.Project = Project;
        //routers:
        this.httpRouter = new ProjectsHttpRouter(this);
        //bind functions:
        this.search = this.search.bind(this);
    }
    search(searchQuery, limit = 10, offset = 0)
    {
        return new Promise((resolve, reject) =>
        {
            if (isEmptyString(searchQuery))
            {
                reject('search query cant be empty');
                return;
            }
            var regex = { '$regex': searchQuery, '$options': 'i' };
            console.log("regix")
            console.log(regix)
            this.Project.find({
                '$or': [
                    { title: regex },
                    { image: regex },
                    { user_id: regex }
                ]
            }).limit(limit).skip(offset).exec((err, results) =>
            {
                if (err)
                {
                    reject(err.toString());
                    return;
                }
            
                resolve(results);
            });
        });
    }
    
}

module.exports = ProjectsHandler;
