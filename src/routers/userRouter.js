var APIRouter = require('./apiRoutes')
const isEmptyString = require('./../utils/utils').isEmptyString;
const moment_now = require('./../utils/utils').moment_now;


class UserHttpRouter extends APIRouter
{
    constructor(handler)
    {
        super();
        this.handler = handler;
        this.router.get('/search', (req, res) =>
        {
            let limit = parseInt(req.query.limit ? req.query.limit : 20);
            let offset = parseInt(req.query.offset ? req.query.offset : 0);
            delete(req.query.limit);
            delete(req.query.offset);
            this.handler.search(req.query.s,limit,offset).then((result) =>
            {
                this.sendResponse(req, res, result);
            }).catch((err) =>
            {
                this.handleError(req, res, err);
            });
        });
    }
}

class UsersHandler
{
     constructor(User)
    {
        this.User = User;
        //routers:
        this.httpRouter = new UserHttpRouter(this);
        //bind functions:
        this.search = this.search.bind(this);
    }
    search(searchQuery, limit = 10, offset = 0)
    {
        return new Promise((resolve, reject) =>
        {
            if (isEmptyString(searchQuery))
            {
                reject('search query cant be empty');
                return;
            }
            var regex = { '$regex': searchQuery, '$options': 'i' };
            console.log("regix")
            console.log(regix)
            this.User.find({
                '$or': [
                    { first_name: regex },
                    { last_name: regex },
                    { phone: regex },
                    { email: regex },
                ]
            }).limit(limit).skip(offset).exec((err, results) =>
            {
                if (err)
                {
                    reject(err.toString());
                    return;
                }
            
                resolve(results);
            });
        });
    }
    
}
module.exports=UsersHandler;

// export class UsersAuthHandler
// {
//     constructor(User)
//     {
//         this.User = User;
//         //routers:
//         this.httpRouter = new UsersAuthHttpRouter(this);
//         //bind functions:
//         // this.login = this.login.bind(this);
//         // this.signup = this.signup.bind(this);
//         // this.editProfile = this.editProfile.bind(this);
      
     
//     }
    // login(params = { username: '', email: '', password: '', phoneNumber: '' })
    // {
    //     return new Promise((resolve, reject) =>
    //     {
    //         if ((isEmptyString(params.username) && isEmptyString(params.email) && isEmptyString(params.phoneNumber) && isEmptyString(params._id)) || isEmptyString(params.password))
    //         {
    //             reject({ code: 500, error: "Parameters Missing! Please Provide _id/email/username/phoneNumber + password!" });
    //             return;
    //         }
    //         const query = {
    //             password: params.password,

    //         };
    //         if (params._id)
    //             query._id = params._id;
    //         if (params.email)
    //             query.email = params.email;
    //         if (params.phoneNumber)
    //             query.phoneNumber = params.phoneNumber;
    //         if (params.username)
    //             query.username = { $regex: new RegExp(params.username, 'i') };
    //         this.User.findOne(query).exec((err, result) =>
    //         {
    //             if (err)
    //             {
    //                 reject({ code: 500, error: err });
    //                 return;
    //             }
    //             if (result == null)
    //             {
    //                 reject({ code: 404, error: "user not found" });
    //                 return;
    //             }
    //             result.token = encoder.encode({ _id: result._id, username: result.username, expiresIn: Date.now() + 14400000 }); //14400000
    //             let d = { token: result.token, lastLogin: moment().format('YYYY-MM-DD hh:mm:ss') };
    //             // if (result.social == undefined)
    //             // {
    //             //     d.social = {
    //             //         followers: [],
    //             //         followings: [],
    //             //         coins: 0,
    //             //         followedHashtags: [],
    //             //     };

    //             // }
    //             // else
    //             // {

    //             // }
    //             this.User.findByIdAndUpdate(result._id, { $set: d }, { new: true }, (err, user) =>
    //             {
    //                 if (err)
    //                 {
    //                     reject({ code: 500, error: err.toString() });
    //                     return;
    //                 }
    //                 if (user == null)
    //                 {
    //                     reject({ code: 404, error: "user not found" });
    //                     return;
    //                 }
    //                 user = user.toObject();
    //                 if (isEmptyString(user.profileImage))
    //                     user.profileImage = SITE_URL('/images/mario-gamer.jpg');
    //                 if (isEmptyString(user.cover))
    //                     user.cover = SITE_URL('/images/user-default-cover.jpg');

    //                 resolve(user);
    //             });
    //         });
    //     });
    // }
    // signup(params)
    // {
    //     return new Promise((resolve, reject) =>
    //     {
    //         if (params.username == undefined || params.password == undefined
    //             || params.phoneNumber == undefined
    //             || params.firstName == undefined || params.lastName == undefined
    //             || params.sex == undefined)
    //         {
    //             reject({ code: 500, error: "Parameters missing!" });
    //             return;
    //         }
    //         const data = {
    //             username: params.username,
    //             password: params.password,
    //             email: params.email ? params.email : "",
    //             phoneNumber: params.phoneNumber,
    //             firstName: params.firstName,
    //             lastName: params.lastName,
    //             epicGamesID: params.epicGamesID ? params.epicGamesID : '',
    //             psnID: params.psnID ? params.psnID : '',
    //             sex: params.sex,
    //             followingGames: [],
    //             createdAt: moment_now(),
    //             refferer: params.refferer ? params.refferer : '',
    //             updatedAt: "",
    //             token: encoder.encode({ _id: '?', username: params.username, expiresIn: Date.now() + 14400000 }), //14400000
    //         }
    //         this.User.findOne({
    //             $or: [
    //                 { username: { $regex: new RegExp(data.username, 'i') } },
    //                 { email: data.email },
    //                 { phoneNumber: data.phoneNumber },
    //             ]
    //         }).exec((err, result) =>
    //         {
    //             if (err)
    //             {
    //                 reject({ code: 500, error: err.toString() });
    //                 return;
    //             }
    //             if (result != null && result != undefined)
    //             {
    //                 var error = "User already exists.";
    //                 if (result.username == data.username)
    //                     error = "User with this username already exists.";
    //                 else if (result.email == data.email)
    //                     error = "User with this email already exists.";
    //                 else if (result.phoneNumber == data.phoneNumber)
    //                     error = "User with this phoneNumber already exists.";
    //                 reject({ code: 500, error: error });
    //                 return;
    //             }
    //             console.log(data);
    //             var doc = new this.User(data);
    //             doc.save().then(() =>
    //             {
    //                 if (isEmptyString(doc.profileImage))
    //                     doc.profileImage = SITE_URL('/images/mario-gamer.jpg');
    //                 if (isEmptyString(doc.cover))
    //                     doc.cover = SITE_URL('/images/user-default-cover.jpg');
    //                 // if (!isEmptyString(params.refferer) && !isEmptyString(params.refferer.replace(' ', '')))
    //                 // {
    //                 //     this.User.findOne({ username: params.refferer }).exec((err, otherUser) =>
    //                 //     {
    //                 //         if (err)
    //                 //         {
    //                 //             console.log('refferer FAILED:' + err.toString());
    //                 //             return;
    //                 //         }
    //                 //         if (otherUser == undefined)
    //                 //         {
    //                 //             return;
    //                 //         }
    //                 //         otherUser.dota2EpicCenter2019.invites.push({ userId: doc._id.toString(), createdAt: moment_now() });
    //                 //         if (otherUser.dota2EpicCenter2019.invites.length < 41)
    //                 //         {
    //                 //             otherUser.dota2EpicCenter2019.coins += 50;
    //                 //             if (otherUser.dota2EpicCenter2019.invites.length == 10)
    //                 //                 otherUser.dota2EpicCenter2019.coins += 250;
    //                 //             else if (otherUser.dota2EpicCenter2019.invites.length == 20)
    //                 //                 otherUser.dota2EpicCenter2019.coins += 500;
    //                 //             else if (otherUser.dota2EpicCenter2019.invites.length == 30)
    //                 //                 otherUser.dota2EpicCenter2019.coins += 750;
    //                 //             else if (otherUser.dota2EpicCenter2019.invites.length == 40)
    //                 //                 otherUser.dota2EpicCenter2019.coins += 1000;
    //                 //         }
    //                 //         this.User.findByIdAndUpdate(otherUser._id, { $set: { dota2EpicCenter2019: otherUser.dota2EpicCenter2019 } }, { new: true }).then((result) =>
    //                 //         {
    //                 //             console.log(`refferer ${result.username} got coins`);
    //                 //         }).catch((err) =>
    //                 //         {
    //                 //             console.log('refferer FAILED:' + err.toString());
    //                 //         });
    //                 //     });
    //                 // }
    //                 resolve(doc);
    //             }).catch((err) =>
    //             {
    //                 reject({ code: 500, error: err.toString() });
    //             });
    //         });
    //     });
    // }

    // editProfile(params)
    // {
    //     return new Promise((resolve, reject) =>
    //     {
    //         const _id = params._id;
    //         if (isEmptyString(params.token))
    //         {
    //             reject('token is missing');
    //             return;
    //         }
    //         this.User.findOne({ token: params.token }).exec((err, poff) =>
    //         {
    //             delete (params.token);
    //             if (err || poff == null)
    //             {
    //                 reject(err ? err : 'object not found');
    //                 return;
    //             }
    //             let editUser = () =>
    //             {
    //                 this.User.findByIdAndUpdate(_id, params, { new: true }).then((result) =>
    //                 {
    //                     //before finishing check if profileImage , cover need resize:
    //                     if (!isEmptyString(result.profileImage))
    //                     {
    //                         //profile image file also exists:
    //                         if (fs.existsSync(path.resolve('..' + result.profileImage)))
    //                         {
    //                             //check resizes:

    //                         }
    //                     }
    //                     resolve(result);
    //                 }).catch((err) =>
    //                 {
    //                     reject(err.toString());
    //                 });
    //             };
    //             if (params.username && poff.username != params.username) 
    //             {
    //                 this.User.findOne({ username: { $regex: new RegExp(params.username, 'i') } }).exec((err, result) =>
    //                 {
    //                     if (err)
    //                     {
    //                         reject(err);
    //                         return;
    //                     }
    //                     if (result != undefined)
    //                         reject("user with this username already exists");
    //                     else
    //                         editUser();
    //                 });
    //             }
    //             else
    //                 editUser();
    //         });
    //     });
    // }


// }