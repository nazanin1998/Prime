var moment = require('moment');
module.exports= function isEmptyString(str)
{
    return str == undefined || str == "undefined" || str == '' || str.replace(' ', '') == '' || str == '?';
}

module.exports= function replaceAll(target, search, replacement)
{
    return target.split(search).join(replacement);
}
module.exports= function moment_now()
{
    return moment().format('YYYY-MM-DD hh:mm:ss');
}

module.exports= function isVideo(str){
    return str.indexOf('.mp4') != -1 || str.indexOf('.avi') != -1 || str.indexOf('.mkv') != -1 || str.indexOf('.webm') != -1;
}