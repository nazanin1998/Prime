// let express = require('express');
// let bodyParser = require('body-parser');
// let mongoose = require('mongoose');

// let app = express();

// let apiRoutes = require("./src/routers/apiRoutes");

// app.use(bodyParser.urlencoded({
//     extended: true
// }));
// app.use(bodyParser.json());

// const DBUrl = "mongodb://localhost/Prime"

// mongoose.connect(DBUrl, { useNewUrlParser: true});

// var db = mongoose.connection;

// if(!db)
//     console.log("Error connecting db")
// else
//     console.log("Db connected successfully")

// // Setup server port
// var port = process.env.PORT || 8080;

// // Send message for default URL
// app.get('/', (req, res) => res.send('Hello World with Express'));

// // Use Api routes in the App
// app.use('/api', apiRoutes);
// // Launch app to listen to specified port
// app.listen(port, function () {
//     console.log("Running Prime on port " + port);
// });

const morgan = require('morgan');
const PORT = 8585;
const MyExpressApp = require('./src/lib/express');
const MONGO_URL =  require('./constant').MONGO_URL;
const express = new MyExpressApp({hasSessionEngine: false,});
const User = require('./src/models/user')
const Project = require('./src/models/project')
const MongooseDB = require('./src/lib/mongooseDb')
const ProjectHanlder = require('./src/routers/projectRouter');
const UserHandler = require('./src/routers/userRouter');
// const APIRouter = require('./src/routers/apiRoutes');
// const apiRoutes = new APIRouter();
const projectHanlder = new ProjectHanlder(Project);
const userHandler = new UserHandler(User);
const PublicMongooseAPIRouter = require("./src/routers/publicMongoose");
express.expressApp.all('*', (req, res, next) =>
{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

console.log('running prime ...');
const db = new MongooseDB(MONGO_URL);
db.schemas.User = User;
db.schemas.Project = Project;
//log middleware:
express.expressApp.use(morgan('tiny'))
//add routers here:
express.expressApp.get('/', (req, res) =>
{
    res.status(200).send('Welcome to API :)');
});
express.expressApp.use('/api/users/', userHandler.httpRouter.router);
express.expressApp.use('/api/users/', new PublicMongooseAPIRouter(User).router);
express.expressApp.use('/api/projects/', projectHanlder.httpRouter.router);
express.expressApp.use('/api/projects/', new PublicMongooseAPIRouter(Project).router);

//listen:
express.http.listen(PORT, function ()
{
    console.log('Prime server listening on port ' + PORT);
});